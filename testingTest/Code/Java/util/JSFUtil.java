package util;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;

import sun.misc.BASE64Decoder;

import com.ibm.xsp.component.UIViewRootEx2;
import com.ibm.xsp.page.compiled.ExpressionEvaluatorImpl;

//import com.ibm.xsp.extlib.util.JdbcUtil;
//import java.sql.Connection;
//import java.sql.SQLException;

import lotus.domino.*;
import lotus.domino.local.NotesBase;

import java.io.*;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.ibm.xsp.extlib.beans.PeopleBean;
import com.ibm.xsp.extlib.util.ExtLibUtil;

public class JSFUtil {

	private JSFUtil() {
	}
	

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getViewScope() {
		return (Map<String, Object>) getVariableValue("viewScope");
	}

	public static Object getBindingValue(final String ref) {
		FacesContext context = FacesContext.getCurrentInstance();
		Application application = context.getApplication();
		return application.createValueBinding(ref).getValue(context);
	}

	public static void setBindingValue(final String ref, final Object newObject) {
		FacesContext context = FacesContext.getCurrentInstance();
		Application application = context.getApplication();
		ValueBinding binding = application.createValueBinding(ref);
		binding.setValue(context, newObject);
	}

	public static Object getVariableValue(final String varName) {
		FacesContext context = FacesContext.getCurrentInstance();
		return context.getApplication().getVariableResolver().resolveVariable(context, varName);
	}

	public static String getUserName() {
		return (String) getBindingValue("#{context.user.name}");
	}

	public static Session getSession() {
		return (Session) getVariableValue("session");
	}

	public static Session getSessionAsSigner() {
		return (Session) getVariableValue("sessionAsSigner");
	}

	public static Database getDatabase() {
		return (Database) getVariableValue("database");
	}

	public static UIViewRootEx2 getViewRoot() {
		return (UIViewRootEx2) getVariableValue("view");
	}

	public static PeopleBean getPeopleBean() {
		return (PeopleBean) getVariableValue("peopleBean");
	}
	
	public static Vector<String> getUserRoles(){
		try {
			return getDatabase().queryAccessRoles(getSession().getEffectiveUserName());
			//.contains(role);
		} catch (NotesException e) {
			return null;
		}
	}

	public static String pluralize(final String input) {
		if (input.endsWith("s")) {
			return input + "es";
		} else if (input.endsWith("y")) {
			return input.substring(0, input.length() - 2) + "ies";
		}
		return input + "s";
	}

	

	public String fetchURL(final String urlString) throws Exception {
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestProperty("User-Agent", "Firefox/2.0");

		BufferedReader in = new BufferedReader(new InputStreamReader((InputStream) conn.getContent()));
		StringWriter resultWriter = new StringWriter();
		String inputLine;
		while ((inputLine = in.readLine()) != null) {
			resultWriter.write(inputLine);
		}
		in.close();

		return resultWriter.toString().replace("<HTTP-EQUIV", "<meta http-equiv");

	}

	public static Map<String, Object> getSessionScope() {
		return (Map<String, Object>) getVariableValue("sessionScope");
	}

	public static void addError(String controlId,String msg){
		FacesContext context = FacesContext.getCurrentInstance();
	    FacesMessage msgObj = new javax.faces.application.FacesMessage(
	              javax.faces.application.FacesMessage.SEVERITY_ERROR, msg, msg
	      );
	    context.addMessage(controlId, msgObj);

	}
	
	public static String getClientId(String id){
		String valueExpr = "#{javascript: getClientId('"+id+"');}";
		FacesContext fc = FacesContext.getCurrentInstance();
		ExpressionEvaluatorImpl evaluator = new ExpressionEvaluatorImpl( fc );
		ValueBinding vb = evaluator.createValueBinding( fc.getViewRoot(), valueExpr, null, null);
		String res= (String) vb.getValue(fc);
		return res;
	}
	
	public static boolean isReadOnly(UIComponent comp){
		FacesContext context = FacesContext.getCurrentInstance();
		return com.ibm.xsp.renderkit.ReadOnlyAdapterRenderer.isReadOnly(context, comp);
	}
}


