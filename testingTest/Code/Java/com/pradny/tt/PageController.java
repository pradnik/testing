package com.pradny.tt;

import java.io.Serializable;

import util.JSFUtil;

import com.ibm.xsp.model.domino.wrapped.DominoDocument;

public class PageController implements Serializable {

	private static final long serialVersionUID = 1L;

	private DominoDocument getDocument() {
		return (DominoDocument) JSFUtil.getVariableValue("document1");
	}
	
	public void doStuff(){
		DominoDocument ddoc = getDocument();
		String v=(String) ddoc.getValue("field1");
		ddoc.setValue("field2",v.toUpperCase());
	}
}
