package com.pradny.test;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.testng.PowerMockTestCase;
import org.testng.annotations.*;

import util.JSFUtil;

import com.ibm.xsp.model.domino.wrapped.DominoDocument;
import static org.mockito.Mockito.*;
import com.pradny.tt.PageController;


@Test
@PrepareForTest(JSFUtil.class)
public class PageControllerTest extends PowerMockTestCase {

	private final static String ANY_STRING="ADASsasdas465456";
	
	PageController pc;
	DominoDocument ddoc;
	
	
	@BeforeMethod
	protected void setUp(){
		ddoc=mock(DominoDocument.class);
		when(ddoc.getValue(anyString())).thenReturn(ANY_STRING);
		
		mockStatic(JSFUtil.class);
		when(JSFUtil.getVariableValue(anyString())).thenReturn(ddoc);
		
		pc=new PageController();
		
	}	
	
	public void shouldGetValueFromDocument(){
		pc.doStuff();
		
		verify(ddoc).getValue(anyString());
		
	}
	
	public void shloudWriteToDocument(){
		pc.doStuff();
		
		verify(ddoc).setValue(anyString(), anyObject());
	}
	
	public void shouldWriteUpperCase(){
		pc.doStuff();
		
		verify(ddoc).setValue(anyString(), eq(ANY_STRING.toUpperCase()));
	}
}
